FROM node:13-alpine

WORKDIR /app

ENV BASE_URL http://localhost:3333

# Install updates and dependencies
RUN apk add python make g++

# Copy application code.
COPY . /app/

# Install dependencies.
RUN yarn && yarn build

# start
CMD ["yarn", "start:prod"]
