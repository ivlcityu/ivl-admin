import { Parser } from 'json2csv'
import { saveAs } from 'file-saver'

export function exportCSV(collection, fields, filename) {
  const parser = new Parser({ fields })
  const csv = parser.parse(collection)
  //export file
  const csvData = new Blob([csv], { type: 'text/csv;charset=utf-8;' })
  saveAs(csvData, `${filename}.csv`)
}
