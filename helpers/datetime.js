import { parse, format } from 'date-fns'

export function formatDatetime(datetime) {
  let initDateTime
  if (datetime instanceof Date) {
    initDateTime = datetime
  } else if (typeof datetime === 'string' || datetime instanceof String) {
    initDateTime = parse(datetime, 'yyyy-MM-dd HH:mm:ss', new Date())
  }
  return format(initDateTime, 'yyyy-MM-dd HH:mm:ss')
}
