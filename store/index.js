export const state = () => ({
  currentCourseId: undefined,
  errorMessage: ''
})

export const mutations = {
  setCurrentCourseId(state, courseId) {
    state.currentCourseId = courseId;
  },
  errorMessage(state, message) {
    state.errorMessage = message
  }
}
